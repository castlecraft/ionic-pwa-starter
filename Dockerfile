FROM node:latest
# Copy and build server
COPY . /home/craft/ionic-pwa-starter
WORKDIR /home/craft/
RUN cd ionic-pwa-starter \
    && npm install \
    && npm run build

FROM nginx:latest

COPY --from=0 /home/craft/ionic-pwa-starter/dist/ionic-pwa-starter /var/www/html
COPY ./docker/nginx-default.conf.template /etc/nginx/conf.d/default.conf.template
COPY ./docker/docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
